﻿//  ***** Owl-sliders *****
$(document).ready(function(){

    $(".main__first-slider .owl-carousel").owlCarousel({
        loop: true,
        margin: 10,
        dots: true,
        items: 1,
        autoplay: true,
        autoplayTimeout: 5000        
    });

    $(".main__second-slider .owl-carousel").owlCarousel({
        loop: true,
        animateOut: 'fadeOut',
        animateIn: 'fadeIn',        
        margin: 10,
        dots: true,
        items: 1,
        autoplay: true,
        autoplayTimeout: 5000,
        
        responsive: {
            1025: {
                onInitialized: callback1,
                onResized: callback,
                onChange: callback,
                mouseDrag: false
            }
        }
    });

    function callback(event) {
        if ( window.innerWidth > 1025 ) {
            let elem = event.target;
            $('.main__second-slider').children('.img-slider').removeClass('fadeIn').addClass('fadeOut');
            setTimeout(function() {
                $('.main__second-slider').children('.img-slider').remove();
                $(elem).find('.owl-item.active').find('.img-slider').clone().appendTo('.main__second-slider').addClass('animated fadeIn');
            }, 100);
        }
    }

    function callback1(event) {
        let elem = event.target;
        $(elem).find('.owl-item.active .img-slider').clone().appendTo('.main__second-slider').addClass('animated fadeIn');
    }
});



// ***** List of cities on main *****
$(window).on('load', function () {

    if ( window.innerWidth <= 1024 ) {
        // ***** UDS link *****
        if ($('.main__uds-info .uds-link').length) {
            $('.main__uds-info .uds-link').remove().appendTo('.main__uds-info');
        }

        // ***** Input file label *****
        if ($('.wrap-input-file').length) {
            $('.wrap-input-file').children('p').remove().prependTo('.wrap-input-file label');
        }
    }

    // ***** Menu *****
    if ( window.innerWidth <= 640 ) {
        if ( $('.menu-panel').length ) {
            $('.menu-panel').each(function() {
                let nameBlock = $(this).find('a').attr('href');
                $(nameBlock).addClass('collapse').remove().appendTo(this);
            });
        }

        if ($('.vacancy-single').length) {
            $('.vacancy-single').each(function() {
                let div = $(this).find('.vacancy-photo');
                $(this).find('.vacancy-title').remove().insertAfter(div);
            });
        }
    }

    try {
        cities();   
    } catch (error) {
        return error;
    }
});

$(window).on('resize', function () {

    if ( window.innerWidth <= 1024 ) {
        $('.main__second-slider').children('.img-slider').remove();
        console.log(123);
    }

    // ***** Input file label *****
    if ($('.wrap-input-file').length) {
        if ( window.innerWidth <= 1024 ) {
            $('.wrap-input-file').children('p').remove().prependTo('.wrap-input-file label');
        } else {
            $('.wrap-input-file .input-file-label').remove().appendTo('.wrap-input-file');
        }
    }

    // ***** Menu *****
    if ( $('.menu-panel').length ) {
        if ( window.innerWidth <= 640 ) {        
            $('.menu-panel').each(function() {
                let nameBlock = $(this).find('a').attr('href');
                $(nameBlock).addClass('collapse').remove().appendTo(this);
            });
        } else {
            $('.menu-panel').each(function() {
                $(this).find('div[id]').removeAttr('style').removeClass('collapse').remove().appendTo('.panel-group');
            });
        }
    }
    

    if ($('.vacancy-single').length) {
        if ( window.innerWidth <= 640 ) {
            $('.vacancy-single').each(function() {
                let div = $(this).find('.vacancy-photo');
                $(this).find('.vacancy-title').remove().insertAfter(div);
            });
        } else {
            $('.vacancy-single').each(function() {
                let div = $(this).find('.vacancy-single__info');
                $(this).find('.vacancy-title').remove().prependTo(div);
            });
        }
    }

    try {
        $('#cities li.last-li-style').removeClass('last-li-style');        
        cities();
        
        // ***** UDS link *****
        if ( window.innerWidth <= 1024 ) {
            $('.main__uds-info .uds-link').remove().appendTo('.main__uds-info');
        } else {
            $('.main__uds-info .uds-link').remove().insertAfter('.main__uds-info .uds-code');
        }
    } catch (error) {
        return error;
    }
});

function cities() {
    let cities = $('#cities li:first').offset().top;
    $('#cities li').each(function() {
        if ( cities < $(this).offset().top ) {
            $($(this).prev()).addClass('last-li-style');
            cities = $(this).offset().top;
        }       
    });
}


// ***** Animation *****
AOS.init();


// ***** Drag&Drop *****
$(document).ready(function() {
    let dropArea = $('#drop-area');

    dropArea.on('dragenter dragover dragleave drop', function(event) {
        event.preventDefault();
        event.stopPropagation();
    });

    dropArea.on('dragenter dragover', function(event) {
        $(this).addClass('highlight');
    });

    dropArea.on('dragleave drop', function(event) {
        $(this).removeClass('highlight');
    });

    dropArea.on('drop', function(event) {
        let files = event.originalEvent.dataTransfer.files;

        handleFiles(files);
    });
});

function handleFiles(files) {    
    $('#fileElem').prop('files', files);
    
    $('#drop-area label > p').not('.drop-area__label').remove();
    if ( $('#fileElem').val() === '' ) {  
        $('#drop-area label > p.drop-area__label').css('display', 'block');
        return;
    }
    $('#drop-area label > p.drop-area__label').css('display', 'none');
    $(files).each(function() {      
        let p = document.createElement('p');
        p.innerHTML = $(this)[0].name;
        $(p).appendTo('#drop-area label');
    });    
}


// ***** Menu item style on scrolling *****
let lastId, 
    topMenuHeight = $('.menu-tabs').outerHeight() + 115,
    menuItems = $('.menu-scroll'),
    scrollItems = menuItems.map(function () {
        var item = $($(this).attr("href"));
        if (item.length) {
            return item;
        }
    });


$(window).on('scroll', function() {
    if ( window.innerWidth > 1024 && $('.menu-scroll').length ) {
        var fromTop = $(this).scrollTop() + topMenuHeight;

        var cur = scrollItems.map(function() {
        if ($(this).offset().top < fromTop)
            return this;
        });
        
        cur = cur[cur.length - 1];
        var id = cur && cur.length ? cur[0].id : "";

        if (lastId !== id) {
            lastId = id;
            menuItems
            .parent().removeClass("active-tab")
            .end().filter("[href='#" + id + "']").parent().addClass("active-tab");
        }
    }
});

$(document).ready(function() {
    $('.menu-panel, .menu-scroll, .toggle-tabs a[data-toggle=tab]').click(function(event) {
        event.preventDefault();

        
        // ***** Collapse *****
        if ( window.innerWidth <= 640 ) {
            $(this).find('li').toggleClass('li-after');
            var collapseContent = $(this).find('a').attr('href');
            $(collapseContent).slideToggle();
        } else {            
            // ***** Tabs *****
            if ( $(this).is("[data-toggle]") ) {
                console.log(this);
                $('.toggle-tabs li').removeClass('active-tab');
                $(this).parent().addClass('active-tab');
                $('.tab-content .tab-pane').removeClass('active-tab');
                var tabContent = $(this).attr('href');
                $('.tab-content').find(tabContent).addClass('active-tab');                
            } else {
                // ***** Menu scroll *****
                var scroll_el = $(this).attr('href');
                    if ($(scroll_el).length != 0) {
                        $('html, body').animate({ scrollTop: $(scroll_el).offset().top - topMenuHeight + 15 }, 500);
                    }
                    return false;
            }
        }

    });
});


// ***** Custom select *****
$('.custom-select').selectric({
    maxHeight: 250
});

$('.custom-select').selectric({    
  onOpen: function() {
    $(this).parent().parent().find('.selectric').addClass('border-style-select');
    $(this).parent().parent().parent().find('label').css('z-index', '10000');
  },
  onClose: function() {
    $(this).parent().parent().find('.selectric').removeClass('border-style-select');
    $(this).parent().parent().parent().find('label').css('z-index', '10');
  }
});



// ***** Input phone mask *****
$(function() {
    $("input[type='phone']").mask("+7 (999) 999-99-99");
});


$(document).ready(function() {
    $('.wrap-label input, .wrap-label textarea').click(function() {
        $(this).parent().find('label').addClass('label-style');
        $(this).css('color', '#000');
    });

    $('.wrap-label input, .wrap-label textarea').blur(function() {
        if ($(this).val() == '') {
            $(this).parent().find('label').removeClass('label-style');
            $(this).css('color', '#fff');
        }
    });
});



// ***** Lib for svg *****
jQuery('img.svg').each(function(){
    var $img = jQuery(this);
    var imgID = $img.attr('id');
    var imgClass = $img.attr('class');
    var imgURL = $img.attr('src');

    jQuery.get(imgURL, function(data) {
        // Get the SVG tag, ignore the rest
        var $svg = jQuery(data).find('svg');

        // Add replaced image's ID to the new SVG
        if(typeof imgID !== 'undefined') {
            $svg = $svg.attr('id', imgID);
        }
        // Add replaced image's classes to the new SVG
        if(typeof imgClass !== 'undefined') {
            $svg = $svg.attr('class', imgClass+' replaced-svg');
        }

        // Remove any invalid XML tags as per http://validator.w3.org
        $svg = $svg.removeAttr('xmlns:a');

        // Check if the viewport is set, if the viewport is not set the SVG wont't scale.
        if(!$svg.attr('viewBox') && $svg.attr('height') && $svg.attr('width')) {
            $svg.attr('viewBox', '0 0 ' + $svg.attr('height') + ' ' + $svg.attr('width'))
        }

        // Replace image with new SVG
        $img.replaceWith($svg);

    }, 'xml');

});
// ***** Validator *****
$(document).ready(function () {
    $("input").change(function () {
        if ($(this).val().length !== 0) {
            $(this).parent().find('label').addClass('label-style');
        } else {
            $(this).parent().removeClass("valid").removeClass("invalid");
        }
    })
    $('#dl-name, #name').on('input', function () {
        var input = $(this);
        var is_name = input.val();

        if (is_name.length >= 3) { input.parent().removeClass("invalid").addClass("valid"); }
        else { input.parent().removeClass("valid").addClass("invalid"); }

        setTimeout(function () {
            var res = /[^А-Яа-яЁё\s]/g.exec(is_name);
            is_name = is_name.replace(res, '');
            if (res) {
                input.parent().removeClass("valid").addClass("invalid");
            }
        }, 0);
    });

    $('form input[type="email"]').blur(function () {
        var email = $(this).val();
        var re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/igm;
        if (re.test(email)) {
            $(this).parent().removeClass("invalid").addClass("valid");
        } else {
            $(this).parent().removeClass("valid").addClass("invalid");
        }

    });

    $('#dl-street').on('input', function () {
        var input = $(this);
        var is_street = input.val();

        if (is_street.length >= 6) { input.parent().removeClass("invalid").addClass("valid"); }
        else { input.parent().removeClass("valid").addClass("invalid"); }

        setTimeout(function () {
            var res = /[^ 0-9А-Яа-яЁё\s\.\-]/g.exec(is_street);
            is_street = is_street.replace(res, '');
            if (res) {
                input.parent().removeClass("valid").addClass("invalid");
            }
        }, 0);
    });
    $('#dl-phone, #phone').change(function () {
        var input = $(this);
        var phone = input.val();

        if ((phone.length <= 18) && (phone.length >= 1)) { input.parent().removeClass("invalid").addClass("valid"); }
        else { input.parent().removeClass("valid").addClass("invalid"); }
    });
    $('#dl-number').on('input', function () {
        var input = $(this);
        var is_number = input.val();

        if ((is_number.length <= 7) && (is_number.length >= 1)) { input.parent().removeClass("invalid").addClass("valid"); }
        else { input.parent().removeClass("valid").addClass("invalid"); }

        setTimeout(function () {
            var res = /[^0-9А-Яа-яЁё\/]/g.exec(is_number);
            is_number = is_number.replace(res, '');
            if (res) {
                input.parent().removeClass("valid").addClass("invalid");
            }
        }, 0);
    });
    $('#dl-kvartira, #dl-podiezd, #dl-floor, #dl-razmen').on('input', function () {
        var input = $(this);
        var is = input.val();

        if ((is.length <= 4) && (is.length >= 1)) { input.parent().removeClass("invalid").addClass("valid"); }
        else { input.parent().removeClass("valid").addClass("invalid"); }

        setTimeout(function () {
            var res = /[^0-9]/g.exec(is);
            is = is.replace(res, '');
            if (res) {
                input.parent().removeClass("valid").addClass("invalid");
            }
        }, 0);
    });

    $('input[type=radio][name=platezh]').change(function () {
        if (this.value == 'platezhcash') {
            $('.input-razmen,.checkbox-element').removeClass('hide');
        }
        else if (this.value == 'platezhcart') {
            $('.input-razmen,.checkbox-element').addClass("hide");
        }
    });



    $("input:required").change(function () {
        var $submit = $('#dl-form-submit'),
            $req = $('form#dlform').find('input:required'),
            $length = $req.length,
            $valid_length = $req.parent('.valid').length;

        if ($length === $valid_length) {
            $('#dl-form-submit').prop('disabled', false);
        }

        if ($length !== $valid_length) {
            $('#dl-form-submit').prop('disabled', true);
        }

        if ($req.val() === '') {
            $('#dl-form-submit').prop('disabled', true);
        }
    });

    $("input#nosdacha.checkbox").change(function () {
        if ($(this).prop('checked')) {
            $('input#dl-razmen').prop('disabled', true);
            $('input#dl-razmen').val('');
            $('.input-razmen label').removeClass('label-style');
            $('input#dl-razmen').parent().removeClass("valid").removeClass("invalid");
        }
        else {
            $('input#dl-razmen').prop('disabled', false);
            $('input#dl-razmen').val('');
            $('input#dl-razmen').parent().removeClass("valid").removeClass("invalid");
        }
    });
});
